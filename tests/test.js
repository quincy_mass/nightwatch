// EXAMPLE NOTE: To preserve the session on test failure and to get session id in afterEach
// add "end_session_on_fail" : false to test_settings in the nightwatch.json.
// Always call .end() to close the session.
module.exports = {
afterEach : function(browser, done) {
console.log("VIDEO_URL: https://s3-us-west-1.amazonaws.com/......./play.html?"+browser.sessionId);
  browser.sessionLog(function(result) {
    console.log(result);
  }).end(function() {
    done();
  });
},
  'Test Google' : function (browser) {
    browser
      .maximizeWindow() // If on Linux set window size, max 1920x1080, like .resizeWindow(1920, 1080);
      .url('http://www.google.com/ncr')
      .waitForElementVisible('body', 1000)
      .setValue('input[type=text]', 'nightwatchjs')
      .waitForElementVisible('button[name=btnG]', 1000)
      .click('button[name=btnG]')
      .pause(1000)
      .assert.containsText('mainx', 'News feed')
  }
};